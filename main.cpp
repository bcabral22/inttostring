/*Brian Cabral
 * user will input an integer between 1-100.
 * any other input will be rejected.
 * if input is valid it will display the number in a word
 * Include myInt.h */

#include "myInt.h"
#include <iostream>
#include <string>
#include <cstdlib>

//declare functions 
int checkNumber(int *userData);
int stringToInt(std::string userInput);
//start of main
int main(){
	//create user Input
	int userNum;
	//while loop. keep looping until valid input
	while(checkNumber(&userNum));
	//create allocated memory in head of an object pointer
	myInt *intToWord = new myInt(userNum);
	//diplay solution
	intToWord->display();
	//delete the memory and set it to Null
	delete intToWord;
	intToWord=NULL;

	return 0;
}
//verify user input is valid and within in the given range of 1-100
int checkNumber(int *userData){
	//create string for userInput
	std::string userInput;
	bool valid= true;
	std::cout<<"Please enter an integer  betwen 1-100: "<<std::endl;
	std::cin>>userInput;
	//range base loop creating char data types from string
	//for(char const& c: userInput){ Range base loop does not work in this version of g++. converted to iterative for loop
	for(int i=0;i<userInput.size();i++){
		char c=userInput[i];
		if(isdigit(c)){
			continue;
		}else{
			valid=false;
	}		break;
	}if(valid){
		//convert sstring into an int
		*userData=stringToInt(userInput);
		//if between 1-100 success
		if(*userData>=1&&*userData<=100){
			return EXIT_SUCCESS;
		}else{
			return EXIT_FAILURE;
		}
	}else{
		return EXIT_FAILURE;
	}
}	

//method to make string into an integer
int stringToInt(std::string userInput){
	int i=0,sum=0;
	//while string is not empty
	while(userInput[i]!='\0'){
		//if it not between the asscii values it can not be converted, else convert to integer
		if(userInput[i]<48 || userInput[i]>57){
			std::cout<<"Not an integer conversion. \n";
			return 0;
		}else{
			sum=sum*10+(userInput[i]-48);
			i++;
		}
	}
	return sum;
}



